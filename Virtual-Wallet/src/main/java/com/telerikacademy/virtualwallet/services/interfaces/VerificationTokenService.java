package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.VerificationToken;

public interface VerificationTokenService {
    void create(User user, String token);

    VerificationToken getVerificationToken(String token);
}