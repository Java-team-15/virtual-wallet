package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.UserTransactionDto;
import com.telerikacademy.virtualwallet.repositories.implementations.pagination.PaginationTransactionRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.TransactionRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {
    private static final String MODIFY_TRANSACTION_ERROR_MESSAGE = "Only user can make transaction.";
    private static final String GET_TRANSACTION_ERROR_MESSAGE = "Only admin can get all transactions.";
    private static final String INVALID_ROLE_ERROR_MESSAGE = "Only admin can get all user's transactions and all users are able to get only their transactions." ;

    private final TransactionRepository repository;
    private final UserRepository userRepository;
    private final WalletService walletService;
    private final TransactionMapper transactionMapper;
    private final PaginationTransactionRepository paginationTransactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository repository, UserRepository userRepository,
                                  WalletService service,
                                  TransactionMapper transactionMapper,
                                  PaginationTransactionRepository paginationTransactionRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.walletService = service;
        this.transactionMapper = transactionMapper;
        this.paginationTransactionRepository = paginationTransactionRepository;
    }

    @Override
    public Transaction getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Transaction getByField(String field,String value) {
        return repository.getByField(field,value);
    }

    @Override
    public void create(Transaction transaction, User user) {
        if (!user.isUser("user")) {
            throw new UnauthorizedOperationException(MODIFY_TRANSACTION_ERROR_MESSAGE);
        }
        walletService.update(transaction);
        repository.create(transaction);
    }

    @Override
    public Page<Transaction> getAll(User user,int currentPage) {
        if (!user.isUser("admin")) {
            throw new UnauthorizedOperationException(GET_TRANSACTION_ERROR_MESSAGE);
        }
        Pageable pageable= PageRequest.of((currentPage-1)*7,7);
        return repository.getAll(pageable);
    }

    @Override
    public Page<Transaction> transactionsPagination(Pageable pageable) {
        return paginationTransactionRepository.findAll(pageable);
    }

    @Override
    public long getTotalElements(User authenticated) {
        if (!authenticated.isUser("admin")) {
            throw new UnauthorizedOperationException(GET_TRANSACTION_ERROR_MESSAGE);
        }
        return repository.getTotalElements();
    }

    @Override
    public int getTotalPages(User authenticated) {
        long totalItems=getTotalElements(authenticated);
        int pageSize=7;
        boolean isDevider=(totalItems%pageSize==0);
        int totalPages=(int)totalItems/pageSize;
        if(!isDevider){
            totalPages+=1;
        }
        return totalPages;
    }


    @Override
    public long getTotalElements(User authenticated,int userId,Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date) {
        if(userId!=authenticated.getId() && authenticated.isUser("user")){
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR_MESSAGE);
        }
        Optional<Integer> senderId= Optional.empty();
        Optional<Integer> recipientId=Optional.empty();
        if(sender.isPresent() && !sender.get().equals("")){
            User userSender=userRepository.getByField("username",sender.get());
            senderId=Optional.of(userSender.getId());
        }
        if(recipient.isPresent()&& !recipient.get().equals("")){
            User userRecipient=userRepository.getByField("username",recipient.get());
            recipientId=Optional.of(userRecipient.getId());
        }
        return repository.getTotalElements(userId,fromDate,toDate,senderId,recipientId,amount,date);
    }

    @Override
    public int getTotalPages(User authenticated, int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date) {
        long totalItems=getTotalElements(authenticated,userId,fromDate,toDate,sender,recipient,amount,date);
        int pageSize=5;
        boolean isDevider=(totalItems%pageSize==0);
        int totalPages=(int)totalItems/pageSize;
        if(!isDevider){
            totalPages+=1;
        }
        return totalPages;
    }

    @Override
    public Page<UserTransactionDto> getAllForUser(User authenticated,
                                                  int id,
                                                  int currentPage,
                                                  Optional<LocalDate> fromDate,
                                                  Optional<LocalDate> toDate,
                                                  Optional<String> sender,
                                                  Optional<String> recipient,
                                                  Optional<String> amount,
                                                  Optional<String> date) {
        if(id!=authenticated.getId() && authenticated.isUser("user")){
            throw new UnauthorizedOperationException(INVALID_ROLE_ERROR_MESSAGE);
        }
        Pageable pageable= PageRequest.of((currentPage-1)*5,5);

        Optional<Integer> senderId= Optional.empty();
        Optional<Integer>recipientId=Optional.empty();
        if(sender.isPresent() && !sender.get().equals("")){
            User userSender=userRepository.getByField("username",sender.get());
            senderId=Optional.of(userSender.getId());
        }
       if(recipient.isPresent()&& !recipient.get().equals("")){
           User userRecipient=userRepository.getByField("username",recipient.get());
           recipientId=Optional.of(userRecipient.getId());
       }

        List<UserTransactionDto>transactions= repository.getAllForUser(id,pageable,fromDate, toDate,senderId,recipientId, amount,  date).stream().map(transaction -> {
            UserTransactionDto userTransactionDto = new UserTransactionDto();
            transactionMapper.toUserDto(userTransactionDto, transaction, id);
            return userTransactionDto;
        }).collect(Collectors.toList());
        Page<UserTransactionDto> page = new PageImpl<>(transactions);
        return page;
    }

//    @Override
//    public List<UserTransactionDto> getAllFiltered(User authenticated, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date) {
//
//
//            Optional<Integer> senderId= Optional.empty();
//            Optional<Integer>recipientId=Optional.empty();
//            if(sender.isPresent() && !sender.get().equals("")){
//                User userSender=userRepository.getByField("username",sender.get());
//                senderId=Optional.of(userSender.getId());
//            }
//            if(recipient.isPresent()&& !recipient.get().equals("")){
//                User userRecipient=userRepository.getByField("username",recipient.get());
//                recipientId=Optional.of(userRecipient.getId());
//            }
//
//            return repository.getAllFiltered(authenticated.getId(),fromDate, toDate,senderId,recipientId, amount,  date).stream().map(transaction -> {
//                UserTransactionDto userTransactionDto = new UserTransactionDto();
//                transactionMapper.toUserDto(userTransactionDto, transaction, authenticated.getId());
//                return userTransactionDto;
//            }).collect(Collectors.toList());
//    }


}
