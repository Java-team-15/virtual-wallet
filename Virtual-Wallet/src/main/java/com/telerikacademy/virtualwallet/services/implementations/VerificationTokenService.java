package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.VerificationToken;
import com.telerikacademy.virtualwallet.repositories.interfaces.VerificationTokenRepository;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenService implements UserService.VerificationTokenServiceImpl {
    private final VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public VerificationTokenService(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }


    @Override
    public void create(User user, String token){
        VerificationToken verificationToken=new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);
        verificationToken.setExpiryDate(calculateExpiryDate(24*60));
        verificationTokenRepository.create(verificationToken);
    }

    public VerificationToken getVerificationToken(String token) {
        return verificationTokenRepository.getVerificationToken(token);
    }
}
