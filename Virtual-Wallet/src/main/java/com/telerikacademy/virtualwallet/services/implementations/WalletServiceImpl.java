package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.Wallet;
import com.telerikacademy.virtualwallet.models.enums.Status;
import com.telerikacademy.virtualwallet.repositories.interfaces.WalletRepository;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.InsufficientResourcesException;
import java.math.BigDecimal;

@Service
public class WalletServiceImpl implements WalletService {

    private static final String USER_BLOCKED_ERROR_MESSAGE = "Blocked user is not allowed to create transactions.";
    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }



    @Override
    public Wallet getByUserId(int userId) {
        return walletRepository.getByUserId(userId);
    }

    @Override
    public Wallet getById(int walletId) {
        return walletRepository.getById(walletId);
    }

    @Override
    public void create(User user) {
        Wallet wallet= new Wallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);
        walletRepository.create(wallet);
    }

    @Override
    public void update(Transaction transaction) {
        int sender = transaction.getSender().getId();
        int recipient = transaction.getRecipient().getId();
       if(sender != recipient) {
           Wallet senderWallet = getByUserId(sender);
           if(transaction.getSender().getStatus().equals(Status.valueOf("BLOCK"))){
               throw new UnauthorizedOperationException(USER_BLOCKED_ERROR_MESSAGE);
           }
           if (senderWallet.getBalance().compareTo(transaction.getAmount()) < 0) {
               throw new InsufficientFundsException("not enough funds");
           }
           senderWallet.setBalance(senderWallet.getBalance().subtract(transaction.getAmount()));
           walletRepository.update(senderWallet);
       }
       Wallet recipientWallet = getByUserId(recipient);
       recipientWallet.setBalance(recipientWallet.getBalance().add(transaction.getAmount()));
       walletRepository.update(recipientWallet);
    }

    @Override
    public void update(Wallet wallet) {
        walletRepository.update(wallet);
    }


}
