package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.controllers.CloudinaryHelper;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.repositories.implementations.pagination.PaginationUsersRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final String DELETE_USER_ERROR_MESSAGE = "Access denied";
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only admin can modify user status.";
    private static final String GET_USERS_ERROR_MESSAGE = "Only admin can view details of all users." ;
    private static final String GET_USER_ERROR_MESSAGE = "Only admin can view details of user.";
    private static final String UPDATE_USER_ERROR_MESSAGE = "Users can update only their profile.";

    private final UserRepository repository;
    private final WalletService walletService;
    private final CloudinaryHelper cloudinaryHelper;
    private final PaginationUsersRepository paginationUsersRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository,
                           WalletService walletService,
                           CloudinaryHelper cloudinaryHelper,
                           PaginationUsersRepository paginationUsersRepository) {
        this.repository = repository;
        this.walletService = walletService;
        this.cloudinaryHelper = cloudinaryHelper;
        this.paginationUsersRepository = paginationUsersRepository;
    }

    @Override
    public Page<User> userPagination(Pageable pageable) {
        return paginationUsersRepository.findAll(pageable);
    }

    @Override
    public Page<User> getAll(User user,int currentPage)  {
        if (!user.isUser("admin")) {
            throw new UnauthorizedOperationException(GET_USERS_ERROR_MESSAGE);
        }
        Pageable pageable= PageRequest.of((currentPage-1)*7,7);
        return repository.getAll(pageable);
    }


    @Override
    public Page<User> searchByKeyword(int currentPage,Optional<String> word) {
        Pageable pageable= PageRequest.of((currentPage-1)*5,5);
        return repository.searchByKeyword(pageable,word);
    }

    @Override
    public User getById(int id, User user) {
        if (user.getId()!=id && user.isUser("user")) {
            throw new UnauthorizedOperationException(GET_USER_ERROR_MESSAGE);
        }
        return repository.getById(id);
    }
//    @Override
//    public String getUserStatus(int id, User user) {
//        if (user.getId()!=id && user.isUser("user")) {
//            throw new UnauthorizedOperationException(GET_USER_ERROR_MESSAGE);
//        }
//        return repository.getUserStatus(id);
//    }

    @Override
    public User getByField(String field,String value) {
        return repository.getByField(field,value);
    }

    @Override
    public void create(User user) {

        //check if username is already in use
        boolean duplicateExists = true;

        try {
            repository.getByField("username",user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        //check if email is already in use
        duplicateExists = true;

        try {
            repository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists){
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        //check if phone number is already in use
        duplicateExists = true;

        try{
            repository.getByField("phone_number", user.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }

        repository.create(user);
        walletService.create(user);
    }

    @Override
    public void update(User user,int id) {

        if (user.getId()!=id ) {
            throw new UnauthorizedOperationException(UPDATE_USER_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByField("email", user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        duplicateExists = true;
        try {
            User existingUser = repository.getByField("phone_number", user.getPhoneNumber());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }

        repository.update(user);
    }

    @Override
    public void setStatus(User user, User authenticated) {
        if (!authenticated.isAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        repository.update(user);
    }

    public void verifyUser(User user) {
        repository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(DELETE_USER_ERROR_MESSAGE);
        }
        repository.delete(id);

}
    @Override
    public long getTotalElements(User authenticated, Optional<String> word) {
        if(!authenticated.isUser("admin")){
            throw new UnauthorizedOperationException(GET_USERS_ERROR_MESSAGE);
        }
        return repository.getTotalElements(word);
    }
    @Override
    public int getTotalPages(User authenticated, Optional<String> word) {
        long totalItems=getTotalElements(authenticated,word);
        int pageSize=5;
        boolean isDevider=(totalItems%pageSize==0);
        int totalPages=(int)totalItems/pageSize;
        if(!isDevider){
            totalPages+=1;
        }
        return totalPages;
    }

    @Override
    public long getTotalElements(User authenticated) {
        if(!authenticated.isUser("admin")){
            throw new UnauthorizedOperationException(GET_USERS_ERROR_MESSAGE);
        }
       return repository.getTotalElements();
    }

    @Override
    public int getTotalPages(User authenticated) {
        long totalItems=getTotalElements(authenticated);
        int pageSize=7;
        boolean isDevider=(totalItems%pageSize==0);
        int totalPages=(int)totalItems/pageSize;
        if(!isDevider){
            totalPages+=1;
        }
        return totalPages;
    }

    //    public boolean passwordMeetsRequirements(String password) {
//
//        if (password.length() < 8) {
//            return false;
//        }
//
//        boolean containsDigit = false;
//        boolean containsCapitalLetter = false;
//        boolean containsSpecialCharacter = false;
//
//        char[] charArr = password.toCharArray();
//
//        for (char current: charArr) {
//
//            //ascii values for special characters
//            if (!containsSpecialCharacter) {
//
//                if (current >= 33 && current <= 47
//                        || current >= 58 && current <= 64
//                        || current >= 91 && current <= 96
//                        || current >= 123 && current <= 126) {
//
//                    containsSpecialCharacter = true;
//                }
//            }
//
//            if (!containsCapitalLetter){
//
//                if (Character.isLetter(current) && Character.isUpperCase(current)) {
//                    containsCapitalLetter = true;
//                }
//            }
//
//            if (!containsDigit) {
//
//                if (Character.isDigit(current)){
//                    containsDigit = true;
//                }
//            }
//
//
//            if (containsSpecialCharacter && containsCapitalLetter && containsDigit) {
//                return true;
//            }
//        }
//        return false;
//    }
    @Override
    public void upload(MultipartFile file, User user) throws IOException {
        String url = cloudinaryHelper.upload(file);
        user.setUserPhoto(url);
        repository.update(user);
    }
}
