package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.Wallet;

public interface WalletService {

   void create(User user);

   void update(Transaction transaction);

    void update(Wallet wallet);

    Wallet getByUserId(int userId);

    Wallet getById(int walletId);

}
