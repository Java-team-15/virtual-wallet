package com.telerikacademy.virtualwallet.services.interfaces;


import com.telerikacademy.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserService {
    Page<User> getAll(User user,int currentPage);

    User getById(int id, User user);

    void create(User user);

    void update(User user,int id);

    void setStatus(User user,User authenticated);

    void delete(int id, User user);

    User getByField(String field,String value);
    //String getUserStatus(int id, User user);

    Page<User> searchByKeyword (int currentPage,Optional<String> word);

    Page<User> userPagination(Pageable pageable);
    long getTotalElements(User authenticated, Optional<String> word);
    int getTotalPages(User authenticated, Optional<String> word);
    long getTotalElements(User authenticated);
    int getTotalPages(User authenticated);

//    boolean passwordMeetsRequirements(String password);

    void upload(MultipartFile file, User user)throws IOException;

    interface VerificationTokenServiceImpl {
        void create(User user, String token);

        //expiry date
        default Date calculateExpiryDate(int expiryTimeInMinutes) {
            Calendar cal = Calendar.getInstance();
            Date date = Date.from(Instant.now());
            cal.setTime(date);
            cal.add(Calendar.MINUTE, expiryTimeInMinutes);
            return new Date(cal.getTime().getTime());
        }
    }
}
