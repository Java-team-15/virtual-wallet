package com.telerikacademy.virtualwallet.services.interfaces;

import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.UserTransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionService {

    Transaction getById(int id);

    Transaction getByField(String field, String value);

    void create(Transaction transaction, User user);

    Page<Transaction> getAll(User user,int currentPage);

    Page<Transaction> transactionsPagination(Pageable pageable);

    long getTotalElements(User authenticated);
    int getTotalPages(User authenticated);

    long getTotalElements(User authenticated,int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date);

    int getTotalPages(User authenticated,int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date);
    Page<UserTransactionDto> getAllForUser(User authenticated, int id,int currentPage, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<String> sender, Optional<String> recipient, Optional<String> amount, Optional<String> date);

}
