package com.telerikacademy.virtualwallet.services.implementations;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.Card;
import com.telerikacademy.virtualwallet.repositories.interfaces.CardRepository;
import com.telerikacademy.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    private final CardRepository repository;

    @Autowired
    public CardServiceImpl(CardRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Card> getAll() {
        return repository.getAll();
    }

    @Override
    public Card getByUserId(int id) {
        return repository.getByUserId(id);
    }


    @Override
    public Card getByField(String field,String value) {
        return repository.getByField(field,value);
    }
    @Override
    public void create(Card card) {
        boolean duplicateExists = true;
        try {
            repository.getByField("card_number",card.getCardNumber());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Card", "card number", card.getCardNumber());
        }

        repository.create(card);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }
}
