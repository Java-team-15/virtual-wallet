package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final WalletService walletService;
    private final TransactionService transactionService;
    private final UserService userService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper,
                             WalletService walletService,
                             TransactionService transactionService,
                             UserService userService) {

        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.userService = userService;
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }
    @ModelAttribute("isUser")
    public boolean populateIsUser(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("user");
        }
        return false;
    }


    @ModelAttribute
    public Model getUserTransactions(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return model;
        }
        //fixme
        return model.addAttribute("loggedUser" , userService.getById(user.getId(), user));
    }
    @ModelAttribute
    public Model getDefaultPage(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return model;
        }
        return model.addAttribute("currentPage" , 1);
    }

}
