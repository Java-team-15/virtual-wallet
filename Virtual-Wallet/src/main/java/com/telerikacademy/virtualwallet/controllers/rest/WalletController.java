package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.WalletViewDto;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/wallets")
public class WalletController {


    private final WalletService walletService;
    private final WalletMapper walletMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WalletController(WalletService walletService,
                            WalletMapper walletMapper, AuthenticationHelper helper) {
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.authenticationHelper = helper;
    }

    @GetMapping("/{id}")
    public WalletViewDto getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return walletMapper.toViewDto(walletService.getById(id));
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping
    public WalletViewDto getByUser(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return walletMapper.toViewDto(walletService.getByUserId(user.getId()));
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
