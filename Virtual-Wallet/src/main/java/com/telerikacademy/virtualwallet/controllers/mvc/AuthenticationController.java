package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.VerificationToken;
import com.telerikacademy.virtualwallet.models.dtos.LoginDto;
import com.telerikacademy.virtualwallet.models.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.services.implementations.OnRegistrationCompleteEvent;
import com.telerikacademy.virtualwallet.services.implementations.VerificationTokenService;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {


    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    private final ApplicationEventPublisher eventPublisher;
    private final VerificationTokenService verificationTokenService;

    @Autowired
    public AuthenticationController(UserService userService,
                                    UserMapper userMapper,
                                    AuthenticationHelper authenticationHelper,
                                    ApplicationEventPublisher eventPublisher,
                                    VerificationTokenService verificationTokenService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.eventPublisher = eventPublisher;
        this.verificationTokenService = verificationTokenService;

    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());

            if (!user.isVerified()) {
                return "errors/not-verified";
            }

            session.setAttribute("currentUser", login.getUsername());
            session.setAttribute("role", user.getRole().getName());
            session.setAttribute("status", user.getStatus());
            // fixme: {NEEDS TESTING} setting attribute to a set of roles, not sure if possible nor correct

            return "redirect:/";
            //homeMvcController

        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }


    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto registerDto,
                                 BindingResult bindingResult,
                                 HttpSession session,
                                 HttpServletRequest request,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return  "register";
        }

        try {
            User user = userMapper.fromDto(registerDto);
            userService.create(user);
            String url = request.getContextPath();

            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, request.getLocale(), url));

            model.addAttribute("emailId", user.getEmail());
            return "/auth/register-success";

        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }

    @GetMapping("/registrationConfirm")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {

       

        VerificationToken verificationToken = verificationTokenService.getVerificationToken(token);
        if (verificationToken == null) {
            String message = "";
            model.addAttribute("message", message);
            return "/auth/not-verified";
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = "The token expires 24 hours after being sent.";
            model.addAttribute("message", messageValue);
            return "/errors/expiration-date";
        }

        user.setVerified(true);
        userService.update(user,user.getId());
        return "/auth/account-verified";
    }
}