package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.SearchUserDto;
import com.telerikacademy.virtualwallet.models.dtos.UserDto;
import com.telerikacademy.virtualwallet.models.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.models.enums.Status;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UsersMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper helper;
    private final WalletService walletService;

    @Autowired
    public UsersMvcController(UserService userService,
                              AuthenticationHelper helper,
                              WalletService walletService,
                              UserMapper mapper) {
        this.userService = userService;
        this.helper = helper;
        this.walletService = walletService;
        this.userMapper = mapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }

    @ModelAttribute
    public Model getLoggedUser(HttpSession session, Model model) {
        if (populateIsAuthenticated(session)){
            User loggedUser = helper.tryGetUser(session);
            return model.addAttribute("loggedUser" , userService.getById(loggedUser.getId(), loggedUser));
        }
        return model;
    }


    @GetMapping("/{id}")
    public String showUserAccount(@PathVariable int id,HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = helper.tryGetUser(session);
            User userToView = userService.getById(id, loggedUser);
            UserViewDto viewDto = userMapper.fromObjectToViewDto(userToView);
            model.addAttribute("user", viewDto);
            return "customer-account";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }
    }
    @GetMapping("/{id}/status")
    public String showSingleUser (@PathVariable int id,HttpSession session, Model model) {
        User loggedUser;
        try {
            loggedUser = helper.tryGetUser(session);
            User userToView = userService.getById(id, loggedUser);
            model.addAttribute("userStatus", userToView);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }
    }
    @GetMapping("/{currentPage}/page")
    public String showAllUsers( @PathVariable int currentPage,
                                HttpSession session,
                                HttpServletRequest request,
                                Model model) {
        User user;
        try {
           user = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        var page=userService.getAll(user,currentPage);
        long totalItems=userService.getTotalElements(user);
        int totalPages=userService.getTotalPages(user);
        var users=page.getContent();
        String queryString = request.getQueryString();
        if (queryString==null){
            queryString="";
        }
        model.addAttribute("users", users);
        model.addAttribute("keyword",new SearchUserDto());
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("currentPage",currentPage);
        model.addAttribute("queryString",queryString);
        return "users";
    }

    @GetMapping("/{id}/update")
    public String showUpdateUserPage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user= userService.getById(id, loggedUser);

            model.addAttribute("updateUser", userMapper.toDto(user));
            return "customer-account-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }
    }

    @PostMapping("/{id}/status")
    public String setStatus(@PathVariable int id,
                            HttpSession session,
                            Model model) {
        try {
            User authenticated = helper.tryGetUser(session);
            User user = userMapper.setStatus(id);
            userService.setStatus(user,authenticated);
            model.addAttribute("userStatus",user);
            model.addAttribute("isBlocked",user.getStatus().equals(Status.BLOCK));
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        }  catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("updateUser")UserDto userDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {

        try {
            helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()){
            return "customer-account-update";
        }

//        if (!userService.passwordMeetsRequirements(userDto.getPassword())){
//            bindingResult.rejectValue("password", "password_error", "Password must contain at least 8 symbols, a capital letter, digit and special symbol");
//            return "customer-account-update";
//        }
//
//        if (!userService.passwordMeetsRequirements(userDto.getConfirmPassword())){
//            bindingResult.rejectValue("confirmPassword", "password_error", "Password must contain at least 8 symbols, a capital letter, digit and special symbol");
//            return "customer-account-update";
//        }

        if (!userDto.getPassword().equals(userDto.getConfirmPassword())){
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password Confirm should match password!");
            return "customer-account-update";
        }

        try {
            User userToUpdate = userMapper.fromDto(userDto,id);
            userService.update(userToUpdate,id);
            return "/users/user-updated-successfully";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("phoneNumber", "duplicated_user_info", e.getMessage());
            return "customer-account-update";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(HttpSession session,@PathVariable int id, Model model) {
        try {
            User authenticated=helper.tryGetUser(session);
            userService.delete(id,authenticated);

            return "redirect:/user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/access-denied";
        }
    }

    @GetMapping("/{currentPage}/page/search")
    public String search(HttpSession session,
                         HttpServletRequest request,
                         @ModelAttribute("keyword") SearchUserDto keyword,
                         @PathVariable int currentPage,
                         Model model)
    {
        User authenticated=helper.tryGetUser(session);
        var page=userService.searchByKeyword(currentPage,Optional.of(keyword.getKeyword()));
        var users=page.getContent();
        long totalItems=userService.getTotalElements(authenticated,Optional.of(keyword.getKeyword()));
        int totalPages=userService.getTotalPages(authenticated,Optional.of(keyword.getKeyword()));
        String queryString = request.getQueryString();
        if (queryString==null){
            queryString="";
        }
                model.addAttribute("users",users);
                model.addAttribute("totalItems",totalItems);
                model.addAttribute("totalPages",totalPages);
                model.addAttribute("currentPage",currentPage);
                model.addAttribute("queryString",queryString);
        return "users";
    }

    @GetMapping("/image")
    public String showUploadPage() {
        return "customer-image-update";
    }

    @PostMapping("/image")
    public String upload(MultipartFile file, HttpSession session) throws IOException {
        User user = helper.tryGetUser(session);
        userService.upload(file, user);
        return String.format("redirect:/users/%s",user.getId());
    }

}
