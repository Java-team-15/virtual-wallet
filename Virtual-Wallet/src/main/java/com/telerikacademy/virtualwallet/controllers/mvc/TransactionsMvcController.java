package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.FilterTransactionDto;
import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.services.interfaces.*;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Optional;

@Controller
@RequestMapping("/transactions")
public class TransactionsMvcController {

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper helper;
    private final UserService userService;

    @Autowired
    public TransactionsMvcController(TransactionService service,
                                     TransactionMapper transactionMapper,
                                     AuthenticationHelper helper,
                                     UserService userService) {
        this.transactionService = service;
        this.transactionMapper = transactionMapper;
        this.helper = helper;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }
    @ModelAttribute
    public Model getLoggedUser(HttpSession session, Model model) {
        if (populateIsAuthenticated(session)){
            User loggedUser = helper.tryGetUser(session);
            return model.addAttribute("loggedUser" , userService.getById(loggedUser.getId(), loggedUser));
        }
        return model;
    }


//    @GetMapping("/{id}")
//    public String showSingleTransaction(@PathVariable int id, Model model) {
//        try {
//            Transaction transaction = transactionService.getById(id);
//            model.addAttribute("transaction", transaction);
//            return "transaction";
//        } catch (EntityNotFoundException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors/not-found";
//        }
//    }

    @GetMapping("/{currentPage}/page")
    public String showAllTransactions(@PathVariable int currentPage,
                                      HttpSession session,
                                      HttpServletRequest request,
                                      Model model) {
        User user;
        try {
            user = helper.tryGetUser(session);
            var page=transactionService.getAll(user,currentPage);
            long totalItems=transactionService.getTotalElements(user);
            int totalPages=transactionService.getTotalPages(user);
            var allTransactions=page.getContent();
            String queryString = request.getQueryString();
            if (queryString==null){
                queryString="";
            }
            model.addAttribute("allTransactions", allTransactions);
            model.addAttribute("filterTransactionDto",new FilterTransactionDto());
            model.addAttribute("currentPage",currentPage);
            model.addAttribute("totalItems",totalItems);
            model.addAttribute("totalPages",totalPages);
            model.addAttribute("queryString",queryString);
            return "all-transactions";
        }  catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/access-denied";
        }

    }


//    @GetMapping("/page")
//    public String showPageTransactions(HttpSession session) {
//        return "pagination-transactions";
//    }

    @GetMapping("/{id}/filter/{currentPage}/page")
    public String filterTransaction(@PathVariable int id,
                                    @PathVariable int currentPage,
                                    HttpSession session,
                                    HttpServletRequest request,
                                    @ModelAttribute("filterTransactionDto") FilterTransactionDto filterTransactionDto,
                                    Model model){

        try {
           User authenticated = helper.tryGetUser(session);
            Optional<LocalDate> fromDate=Optional.ofNullable(filterTransactionDto.getFromDate());
            Optional<LocalDate> toDate=Optional.ofNullable(filterTransactionDto.getToDate());
            Optional<String> sender=Optional.ofNullable(filterTransactionDto.getSender());
            Optional<String> recipient=Optional.ofNullable(filterTransactionDto.getRecipient());
            Optional<String> orderAmount=Optional.ofNullable(filterTransactionDto.getOrderAmount());
            Optional<String> orderDate = Optional.ofNullable(filterTransactionDto.getOrderDate());
            var page=transactionService.getAllForUser(authenticated,id,currentPage,fromDate,toDate,sender,recipient,orderAmount,orderDate);
            long totalItems=transactionService.getTotalElements(authenticated,id,fromDate,toDate,sender,recipient,orderAmount,orderDate);

            int totalPages=transactionService.getTotalPages(authenticated,id,fromDate,toDate,sender,recipient,orderAmount,orderDate);
            var listTransactions=page.getContent();

            User userTransaction=userService.getByField("id",String.valueOf(id));

            String queryString = request.getQueryString();
            if (queryString==null){
                queryString="";
            }

            model.addAttribute("customerTransactions",listTransactions);
            model.addAttribute("filterTransactioDto",new FilterTransactionDto());
            model.addAttribute("totalItems",totalItems);
            model.addAttribute("totalPages",totalPages);
            model.addAttribute("currentPage",currentPage);
            model.addAttribute("userId",userTransaction.getId());
            model.addAttribute("queryString",queryString);
            return "customer-transactions";

       } catch (EntityNotFoundException e) {
            model.addAttribute("error",e.getMessage());
            return "errors/not-found";
        }catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

//    @GetMapping("/{id}")
//    public String showAllCustomerTransactions(@PathVariable int id, HttpSession session, Model model,
//                                              @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> fromDate,
//                                              @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> toDate,
//                                              @RequestParam (required = false) Optional<Integer> senderId,
//                                              @RequestParam (required = false) Optional<Integer> recipientId,
//                                              @RequestParam (required = false) Optional<String> amount,
//                                              @RequestParam (required = false) Optional<String> date) {
//        User user;
//
//        try {
//            user = helper.tryGetUser(session);
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/auth/login";
//        }
//
//        //todo
//        model.addAttribute("customerTransactions", transactionService.getAllForUser(user, id, fromDate, toDate, senderId, recipientId, amount, date));
//        model.addAttribute("currentUser", user);
//        return "customer-transactions";
//    }

    @GetMapping("/new")
    public String showCreateTransactionPage(Model model, HttpSession session) {
        try {
            helper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login;";
        }
        model.addAttribute("transaction", new TransactionDto());
        return "transaction-new";
    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute ("transaction") TransactionDto transactionDto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpServletRequest request,
                                    HttpSession session) {
        User user;
        try {
            user = helper.tryGetUser(session);
        }catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()){
            return "transaction-new";
        }

        User recipient;
        try {

            recipient = userService.getByField("username", transactionDto.getRecipient());
            Transaction transaction = transactionMapper.fromDto(transactionDto, user);
            transactionService.create(transaction, user);
            return "/transactions/successful-transaction";
        } catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            bindingResult.rejectValue("recipient", "user with name %s not found", e.getMessage());
            return "transaction-new";
        } catch (InsufficientFundsException e) {
            bindingResult.rejectValue("balance", "insufficient_funds", e.getMessage());
            return "transaction-new";
        } catch (UnauthorizedOperationException e){
            return "errors/blocked-user";
        }
    }

//    @GetMapping("/confirm")
//    public String confirmTransaction(Model model, @RequestParam("token") String confirmationToken) {
//
//        //fixme NEED TO FINISH
//
//        VerificationToken token;
//        User user;
//        try {
//            token = tokenService.findByConfirmationToken(confirmationToken);
//            user = userService.getByField("id", String.valueOf(token.getUserId()));
//
//        } catch (IllegalArgumentException | EntityNotFoundException e) {
//            model.addAttribute("error", e.getMessage());
//            return "errors/not-found";
//        }
//
//        PendingTransaction pendingTransaction = pendingTransactionService.findTransactionByTokenId(token.getId());
//
//        if (pendingTransaction != null) {
//
//            String transactionTokenValue = pendingTransaction.getToken().getConfirmationToken();
//
//
//            if (transactionTokenValue.equals(token.getConfirmationToken())){
//
//                Transaction confirmedTransaction  = transactionMapper.pendingToObject(pendingTransaction);
//
//                pendingTransactionService.delete(pendingTransaction);
//                transactionService.create(confirmedTransaction, user);
//                return "transactions/successful-transaction";
//            }
//        }
//            return "errors/not-found";
//
//
//    }
    //todo: filter
}
