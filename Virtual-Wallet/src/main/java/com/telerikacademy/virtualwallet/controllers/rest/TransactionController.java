package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.models.dtos.UserTransactionDto;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/transactions")
public class TransactionController {
    private final static String URL_BANK = "http://localhost:8050/api/bank";

    private final static String UNAVAILABLE_FUNDS_ERROR_MSG = "Not enough funds in the card";

    private final AuthenticationHelper authenticationHelper;
    private final TransactionService transactionService;
    private final RestTemplate restTemplate;
    private final TransactionMapper transactionMapper;
    private final WalletService walletService;


    @Autowired
    public TransactionController(AuthenticationHelper authenticationHelper,
                                 TransactionService service,
                                 RestTemplate restTemplate,
                                 TransactionMapper transactionMapper,
                                 WalletService walletService) {
        this.authenticationHelper = authenticationHelper;
        this.transactionService = service;
        this.restTemplate = restTemplate;
        this.transactionMapper = transactionMapper;
        this.walletService = walletService;
    }

    @GetMapping
    public Page<Transaction> getPage(Pageable pageable) {
        return transactionService.transactionsPagination(pageable);
    }

//    @GetMapping
//    public List<Transaction> getAll(@RequestHeader HttpHeaders headers) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            return transactionService.getAll(user);
//        }catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

//    @GetMapping("/{id}")
//    public List<UserTransactionDto> getAllForUser(@RequestHeader HttpHeaders headers,
//                                                  @PathVariable int id,
//                                                  @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> fromDate,
//                                                  @RequestParam (required = false) @DateTimeFormat(pattern ="yyyy-MM-dd") Optional<LocalDate> toDate,
//                                                  @RequestParam (required = false) Optional<String> sender,
//                                                  @RequestParam (required = false) Optional<String> recipient,
//                                                  @RequestParam (required = false) Optional<String> amount,
//                                                  @RequestParam (required = false) Optional<String> date) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            return transactionService.getAllForUser(user,id,fromDate,toDate,sender,recipient,amount,date);
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }


    @PostMapping
    public Transaction createTransaction(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransactionDto transactionDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);//sender
            Transaction transaction = transactionMapper.fromDto(transactionDto, user);
            transactionService.create(transaction,user);
            return transaction;
        }catch(InsufficientFundsException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
        catch (EntityNotFoundException e){
           throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }
    @PostMapping("/deposit")
    public Transaction deposit(@RequestHeader HttpHeaders headers, @Positive @RequestBody BigDecimal amount) {
        try {

            User user = authenticationHelper.tryGetUser(headers);//sender
            HttpStatus response = restTemplate.getForObject(URL_BANK, HttpStatus.class);
            if(response != HttpStatus.OK){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,UNAVAILABLE_FUNDS_ERROR_MSG);
            }
            Transaction transaction = transactionMapper.fromDto(amount, user);
            transactionService.create(transaction,user);
            return transaction;
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

}
