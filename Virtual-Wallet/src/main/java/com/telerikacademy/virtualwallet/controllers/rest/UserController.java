package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.models.dtos.UserDto;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public Page<User> getPage(Pageable pageable) {
        return service.userPagination(pageable);
    }

//    @GetMapping
//    public List<User> getAll(@RequestHeader HttpHeaders headers) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            return service.getAll(user);
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//
//        }
//    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers,@PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.getById(id,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());

        }
    }

    @PostMapping
    public User create(@Valid @RequestBody RegisterDto registerDto) {
        try {
            User user = userMapper.fromDto(registerDto);
            service.create(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
    }

    @PutMapping("/{id}/update")
    public User update(@PathVariable int id,
            @RequestHeader HttpHeaders headers,
            @Valid @RequestBody UserDto userDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            user = userMapper.fromDto(userDto, id);
            service.update(user,id);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User setStatus(
            @RequestHeader HttpHeaders headers,
            @PathVariable int id) {
        try {
            User authenticated = authenticationHelper.tryGetUser(headers);
            User user = userMapper.setStatus(id);
            service.setStatus(user,authenticated);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }  catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

//    @GetMapping("/search")
//    public List<User> searchByKeyword( @RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> word) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            return service.searchByKeyword(word);
//        }catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }



}
