package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Card;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.Wallet;
import com.telerikacademy.virtualwallet.models.dtos.DepositDto;
import com.telerikacademy.virtualwallet.models.dtos.WalletViewDto;
import com.telerikacademy.virtualwallet.services.interfaces.CardService;
import com.telerikacademy.virtualwallet.services.interfaces.TransactionService;
import com.telerikacademy.virtualwallet.services.interfaces.UserService;
import com.telerikacademy.virtualwallet.services.interfaces.WalletService;
import com.telerikacademy.virtualwallet.utils.TransactionMapper;
import com.telerikacademy.virtualwallet.utils.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/wallets")
public class WalletMvcController {

    private final static String URL_BANK = "http://localhost:8080/api/bank";
    private final static String UNAVAILABLE_FUNDS_ERROR_MSG = "Not enough funds in the wallet";

    private final WalletService walletService;
    private final AuthenticationHelper authenticationHelper;
    private final RestTemplate restTemplate;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final WalletMapper walletMapper;
    private final CardService cardService;
    private final UserService userService;

    @Autowired
    public WalletMvcController(WalletService walletService,
                               AuthenticationHelper helper,
                               RestTemplate restTemplate,
                               TransactionService transactionService,
                               TransactionMapper transactionMapper,
                               WalletMapper walletMapper,
                               CardService cardService,
                               UserService userService) {
        this.walletService = walletService;
        this.authenticationHelper = helper;
        this.restTemplate = restTemplate;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.walletMapper = walletMapper;
        this.cardService = cardService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute
    public Model getLoggedUser(HttpSession session, Model model) {
        if (populateIsAuthenticated(session)){
            User loggedUser = authenticationHelper.tryGetUser(session);
            return model.addAttribute("loggedUser" , userService.getById(loggedUser.getId(), loggedUser));
        }
        return model;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (populateIsAuthenticated(session)){
            return session.getAttribute("role").toString().equalsIgnoreCase("admin");
        }
        return false;
    }


//    @ModelAttribute
//    public Model getUserWallet(HttpSession session, Model model) {
//        User user;
//        try {
//            user = authenticationHelper.tryGetUser(session);
//        } catch (AuthenticationFailureException e) {
//            return model;
//        }
//        return model.addAttribute("userWallet", walletService.getByUserId(user.getId()));
//    }

    @GetMapping("/user")
    public String showSingleWallet(
                                   Model model,
                                   HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Wallet wallet = walletService.getByUserId(user.getId());

            WalletViewDto walletViewDto = walletMapper.toViewDto(wallet);

            model.addAttribute("displayUserWallet", walletViewDto);
            model.addAttribute("user", user);

            return "wallet";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/access-denied";
        }
    }

    @GetMapping("/deposit")
    public String showDepositPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Card userCard = cardService.getByUserId(user.getId());
        } catch (EntityNotFoundException e) {
            return "redirect:/cards/add";
        }

        model.addAttribute("deposit", new DepositDto());
        return "deposit";
    }

    @PostMapping("/deposit")
    public String handleDeposit(@Valid @ModelAttribute ("depositDto") DepositDto depositDto,
                                   BindingResult bindingResult,
                                   Model model,
                                   HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "deposit";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            HttpStatus response = restTemplate.getForObject(URL_BANK, HttpStatus.class);

            if (response == HttpStatus.OK) {
                Transaction transaction =transactionMapper.fromDepositDto(depositDto, user);
                transactionService.create(transaction,user);
                return "/transactions/successful-deposit";
            } else {
                return "/errors/not-enough-funds";
            }

        } catch (UnauthorizedOperationException e) {
             model.addAttribute("error", e.getMessage());
             return "/errors/access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "/errors/not-found";
        }
    }

}
