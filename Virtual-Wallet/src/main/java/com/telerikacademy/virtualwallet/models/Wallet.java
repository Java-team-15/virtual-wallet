package com.telerikacademy.virtualwallet.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "amount")
    private BigDecimal balance;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Wallet(int id, BigDecimal balance, User user) {
        this.id = id;
        this.balance = balance;
        this.user = user;
    }

    public void depositToWallet(BigDecimal amount) {
        BigDecimal currentBalance = getBalance();
        BigDecimal newBalance = currentBalance.add(amount);
        setBalance(newBalance);
    }

    public void withdrawFromWallet(BigDecimal amount) {
        BigDecimal currentBalance = getBalance();
        BigDecimal newBalance =currentBalance.subtract(amount);
        setBalance(newBalance);
    }

    public Wallet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
