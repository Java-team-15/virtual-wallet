package com.telerikacademy.virtualwallet.models.enums;

public enum CardType {
    CREDIT,
    DEBIT;

    public String toString() {
        switch (this) {
            case CREDIT:
                return "credit";
            case DEBIT:
                return "debit";
            default:
                return "";
        }
    }
}
