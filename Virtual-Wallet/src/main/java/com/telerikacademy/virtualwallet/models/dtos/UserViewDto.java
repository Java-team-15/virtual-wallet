package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;

public class UserViewDto {

    @NotEmpty(message = "username can't be empty")
    private String username;

    @NotEmpty(message = "email can't be empty")
    private String email;

    @NotEmpty (message = "phoneNumber can't be empty" )
    private String phoneNumber;
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public UserViewDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
