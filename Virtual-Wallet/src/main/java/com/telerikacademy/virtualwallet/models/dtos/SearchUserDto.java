package com.telerikacademy.virtualwallet.models.dtos;

public class SearchUserDto {
    private String keyword;

    public SearchUserDto(String keyword) {
        this.keyword = keyword;
    }

    public SearchUserDto() {

    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
