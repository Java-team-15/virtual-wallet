package com.telerikacademy.virtualwallet.models.dtos;

import com.telerikacademy.virtualwallet.models.enums.Direction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.LocalDate;

public class UserTransactionDto {
    @Positive(message = "ID should be positive.")
    private int id;
    @PositiveOrZero(message = "SenderID should be positive or zero.")
    private String sender;

    @PositiveOrZero(message = "RecipientID should be positive or zero.")
    private String recipient;

    @DateTimeFormat(pattern ="yyyy-MM-dd")
    private LocalDate transactionDate;

    @Positive(message = "Balance should be positive.")
    private BigDecimal balance;

    @Enumerated(EnumType.STRING)
    private Direction direction;

    public UserTransactionDto() {

    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
