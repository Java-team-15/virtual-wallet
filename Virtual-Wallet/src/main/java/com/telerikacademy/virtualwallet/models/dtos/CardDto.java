package com.telerikacademy.virtualwallet.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CardDto {

    @NotBlank(message = "Card number can't be empty")
    @Pattern(regexp="\\d{16}",message = "Card number must be exactly 16 digits")
    private String cardNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expirationDate;

    @NotBlank(message = "Card holder can't be empty")
    @Size(min = 2, max = 30, message = "Card holder must be between 2 and 30 characters")
    private String cardHolder;

    @NotBlank(message = "Check number can't be empty")
    @Pattern(regexp="\\d{3}",message = "Check number must be exactly 3 digits")
    private String checkNumber;

    public CardDto() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }


}
