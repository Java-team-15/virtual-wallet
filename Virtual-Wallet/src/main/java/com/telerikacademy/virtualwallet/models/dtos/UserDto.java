package com.telerikacademy.virtualwallet.models.dtos;

import com.telerikacademy.virtualwallet.models.enums.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto{

    @Size(min = 3,max=20, message = "Username must be between 3 and 20 characters")
    @NotBlank(message = "username can't be empty")
    private String username;

    @NotBlank(message = "Password can't be empty")
    @Size(min = 8, message = "Password must be 8 characters or more")
    private String password;

    @NotBlank(message = "confirm password can't be empty")
    @Size(min = 8, message = "Confirm password must be 8 characters or more")
    private String confirmPassword;

    @NotBlank(message = "Email can't be empty")
    @Size(max=30, message = "Email must not be over 30 characters")
    @Email(message = "Valid email required")
    private String email;

    @NotBlank(message = "Phone number can't be empty")
    @Pattern(regexp="\\d{10}",message = "Phone number must be exactly 10 digits")//10 digits
    private String phoneNumber;

    private Status status;

    private String userPhoto;

    public UserDto() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
