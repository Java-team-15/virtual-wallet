package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

public class WalletDto {

    @PositiveOrZero(message = "Balance should be positive or zero.")
    private BigDecimal balance;

    @PositiveOrZero(message = "UserId should be positive or zero.")
    private int userId;

    public WalletDto() {
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
