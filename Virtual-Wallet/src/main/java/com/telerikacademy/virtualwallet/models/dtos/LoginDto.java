package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDto {


        @NotEmpty(message = "Username can't be empty")
        @Size(min = 3,max=20, message = "Username must be between 3 and 20 characters")
        private String username;

        @Size(min = 8, message = "Password must be 8 characters or more")
        @NotEmpty(message = "Password can't be empty")
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
