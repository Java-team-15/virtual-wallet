package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class DepositDto {

    @Positive(message = "Amount should be positive")
    private  BigDecimal amount;

    public DepositDto() {
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
