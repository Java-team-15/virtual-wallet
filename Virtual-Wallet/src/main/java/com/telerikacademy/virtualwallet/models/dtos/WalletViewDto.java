package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

public class WalletViewDto {

    @PositiveOrZero(message = "Balance should be positive or zero.")
    private BigDecimal balance;

    @NotEmpty(message = "UserName should not be empty.")
    private String username;

    public WalletViewDto() {
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
