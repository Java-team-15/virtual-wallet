package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class TransactionDto {

    @NotEmpty(message = "recipient cannot be empty")
    private String recipient;

   @Positive(message = "Balance should be positive.")
    private BigDecimal balance;

    public TransactionDto() {
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}
