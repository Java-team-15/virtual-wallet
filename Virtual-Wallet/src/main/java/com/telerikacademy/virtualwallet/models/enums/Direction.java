package com.telerikacademy.virtualwallet.models.enums;

public enum Direction {
    INCOMING,
    OUTGOING;

    public String toString() {
        switch (this) {
            case INCOMING:
                return "incoming";
            case OUTGOING:
                return "outgoing";
            default:
                return "";
        }
    }
}
