package com.telerikacademy.virtualwallet.utils;

import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.dtos.DepositDto;
import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.models.dtos.UserTransactionDto;
import com.telerikacademy.virtualwallet.models.enums.Direction;
import com.telerikacademy.virtualwallet.repositories.interfaces.TransactionRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class TransactionMapper {
    private final TransactionRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public TransactionMapper(TransactionRepository repository, UserRepository userRepository) {
        this.repository=repository;
        this.userRepository = userRepository;
    }

    public Transaction fromDto(TransactionDto transactionDto,User user) {
        Transaction transaction= new Transaction();
        dtoToObject(transactionDto, transaction,user);
        return transaction;
    }
    public Transaction fromDto(BigDecimal amount,User user) {
        Transaction transaction= new Transaction();
        depositDtoToObject(amount, transaction,user);
        return transaction;
    }


    public Transaction fromDepositDto(DepositDto depositDto, User user) {
        Transaction transaction = new Transaction();
        depositDtoToTransactionObject(depositDto, transaction, user);
        return transaction;
    }

//    public PendingTransaction DtoToPending(TransactionDto transactionDto, User sender, User recipient, VerificationToken token){
//        PendingTransaction pendingTransaction = new PendingTransaction();
//        pendingTransaction.setRecipient(recipient);
//        pendingTransaction.setSender(sender);
//        pendingTransaction.setAmount(transactionDto.getBalance());
//        pendingTransaction.setToken(token);
//        return pendingTransaction;
//    }
//
//    public Transaction pendingToObject(PendingTransaction pendingTransaction){
//        Transaction confirmedTransaction = new Transaction();
//        confirmedTransaction.setRecipient(pendingTransaction.getRecipient());
//        confirmedTransaction.setSender(pendingTransaction.getSender());
//        confirmedTransaction.setAmount(pendingTransaction.getAmount());
//        confirmedTransaction.setTransactionDate(LocalDate.now());
//        return confirmedTransaction;
//    }

    private void depositDtoToTransactionObject(DepositDto depositDto, Transaction transaction, User user) {
        transaction.setAmount(depositDto.getAmount());
        transaction.setSender(user); //todo: when wanting to filter by deposit search if sender is null (coming from bank)
        transaction.setRecipient(user);
        transaction.setTransactionDate(LocalDate.now());
    }

    private void depositDtoToObject(BigDecimal amount, Transaction transaction, User user) {

        User sender = user;
        transaction.setSender(user);
        transaction.setRecipient(user);
        transaction.setTransactionDate(LocalDate.now());
        transaction.setAmount(amount);
    }
    private void dtoToObject(TransactionDto transactionDto, Transaction transaction,User user) {

        transaction.setSender(user);
        User recipient=userRepository.getByAllFields(transactionDto.getRecipient());
        transaction.setRecipient(recipient);
        transaction.setTransactionDate(LocalDate.now());
        transaction.setAmount(transactionDto.getBalance());
    }
    public void toUserDto(UserTransactionDto userTransactionDto, Transaction transaction,int userId) {

        userTransactionDto.setId(transaction.getId());
        userTransactionDto.setSender(transaction.getSender().getUsername());
        userTransactionDto.setRecipient(transaction.getRecipient().getUsername());
        userTransactionDto.setTransactionDate(transaction.getTransactionDate());
        userTransactionDto.setBalance(transaction.getAmount());
        if(transaction.getRecipient().getId()==userId){
            userTransactionDto.setDirection(Direction.INCOMING);
        }
        else {
            userTransactionDto.setDirection(Direction.OUTGOING);
        }
    }




}