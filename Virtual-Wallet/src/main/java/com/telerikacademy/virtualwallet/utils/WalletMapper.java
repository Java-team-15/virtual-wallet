package com.telerikacademy.virtualwallet.utils;

import com.telerikacademy.virtualwallet.models.Wallet;
import com.telerikacademy.virtualwallet.models.dtos.WalletViewDto;
import com.telerikacademy.virtualwallet.repositories.interfaces.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WalletMapper {

    private final WalletRepository walletRepository;

    @Autowired
    public WalletMapper(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    public WalletViewDto toViewDto(Wallet wallet) {
        WalletViewDto viewDto = new WalletViewDto();
        viewDto.setBalance(wallet.getBalance());
        viewDto.setUsername(wallet.getUser().getUsername());
        return viewDto;
    }
}
