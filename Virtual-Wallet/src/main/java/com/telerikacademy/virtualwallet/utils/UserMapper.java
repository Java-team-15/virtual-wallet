package com.telerikacademy.virtualwallet.utils;

import com.telerikacademy.virtualwallet.models.Role;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.Wallet;
import com.telerikacademy.virtualwallet.models.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.models.dtos.UserDto;
import com.telerikacademy.virtualwallet.models.dtos.UserViewDto;
import com.telerikacademy.virtualwallet.models.enums.Status;
import com.telerikacademy.virtualwallet.repositories.interfaces.RoleRepository;
import com.telerikacademy.virtualwallet.repositories.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class UserMapper {
    private static final Status initialStatus = Status.UNBLOCK;
    private static final Status finalStatus = Status.BLOCK;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }
    public UserViewDto fromObjectToViewDto(User user) {
        UserViewDto viewDto = new UserViewDto();
        viewDto.setUsername(user.getUsername());
        viewDto.setEmail(user.getEmail());
        viewDto.setPhoneNumber(user.getPhoneNumber());
        viewDto.setPhoto(user.getUserPhoto());
        return viewDto;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        objectToDto(user, userDto);
        return userDto;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }



    public User setStatus(int id) {
        User user = userRepository.getById(id);
        if (user.getStatus() != finalStatus) {
            user.setStatus(Status.values()[user.getStatus().ordinal() + 1]);
        } else {
            user.setStatus(initialStatus);
        }
        return user;
    }

    public void verifyUser(User user) {
        user.setVerified(true);
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail().toLowerCase());
        user.setPhoneNumber(registerDto.getPhoneNumber());

        Role role = new Role();
        role.setId(2);
        role.setName("User");
        user.setRole(role);

        user.setStatus(Status.UNBLOCK);
        String defoultPhoto="http://res.cloudinary.com/dfxqmj84m/image/upload/v1633735237/nbserpga6wcrvnrm3tif.jpg";
        user.setUserPhoto(defoultPhoto);
        user.setVerified(false);

        Wallet wallet=new Wallet();
        wallet.setUser(user);
        wallet.setBalance(BigDecimal.ZERO);

        return user;
    }

    private void objectToDto(User user, UserDto userDto) {
        userDto.setUserPhoto(user.getUserPhoto());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setConfirmPassword(user.getPassword());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setStatus(user.getStatus());
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setStatus(Status.UNBLOCK);
    }

}
