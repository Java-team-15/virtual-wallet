package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.repositories.interfaces.TransactionRepository;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.thymeleaf.expression.Lists;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {
    private final SessionFactory sessionFactory;

    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Transaction getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.get(Transaction.class, id);
            if (transaction == null) {
                throw new EntityNotFoundException("Transaction", id);
            }
            return transaction;
        }
    }

    @Override
    public Transaction getByField(String field, String value) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery(String.format("from Transaction where %s = '%s'", field, value), Transaction.class);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Transaction", field, value);
            }

            return query.list().get(0);
        }

    }

    @Override
    public void create(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {
            session.save(transaction);
        }
    }

    @Override
    public Page<Transaction> getAll(Pageable pageable) {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction", Transaction.class);
            query.setFirstResult(pageable.getPageNumber());
            query.setMaxResults(pageable.getPageSize());

            List<Transaction>transactions=query.list();
            Page<Transaction> returnPage = new PageImpl<>(transactions);
            return returnPage;
        }
    }
    @Override
    public String getQueryForUser(int userId,
                                  Optional<LocalDate> fromDate,
                                  Optional<LocalDate> toDate,
                                  Optional<Integer> senderId,
                                  Optional<Integer> recipientId,
                                  Optional<String> amount,
                                  Optional<String> date){
        String queryString = "from Transaction where (:recipientId != 0 or sender.id = :userId or recipient.id = :userId) and (:recipientId = 0 or :recipientId = :userId and recipient.id = :recipientId or recipient.id = :recipientId and sender.id = :userId) and (:senderId = 0 or sender.id = :senderId)  and (:fromDate = null or transactionDate >= :fromDate)  and (:toDate = null or transactionDate <= :toDate)";
        if(amount.isPresent() && (amount.equals(Optional.of("asc")) || amount.equals(Optional.of("desc"))) && date.equals(Optional.of(""))){
            queryString+=" order by amount " + amount.get();
        }
        if(date.isPresent() && (date.equals(Optional.of("asc")) || date.equals(Optional.of("desc"))) && amount.equals(Optional.of(""))){
            queryString+=" order by transactionDate " + date.get();//order by amount asc, date desc
        }
        if(amount.isPresent() && date.isPresent()) {
            if ((amount.equals(Optional.of("asc")) || amount.equals(Optional.of("desc"))) && (date.equals(Optional.of("asc")) || date.equals(Optional.of("desc")))) {
                queryString += " order by amount " + amount.get() + ", transactionDate " + date.get();
            }
        }

        return queryString;
    }

    @Override
    public long getTotalElements(int userId,Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = getQueryForUser(userId,fromDate, toDate, senderId, recipientId, amount, date);

            Query<Transaction> query = session.createQuery(queryString,Transaction.class);

            query.setParameter("fromDate", fromDate.orElse(null));
            query.setParameter("toDate", toDate.orElse(null));
            query.setParameter("userId",userId);
            query.setParameter("senderId", senderId.orElse(0));
            query.setParameter("recipientId", recipientId.orElse(0));

            return query.list().size();
        }
    }
    @Override
    public long getTotalElements() {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction", Transaction.class);
            return query.list().size();
        }
    }
    @Override
    public Page<Transaction> getAllForUser(int userId,
                                           Pageable pageable,
                                           Optional<LocalDate> fromDate,
                                           Optional<LocalDate> toDate,
                                           Optional<Integer> senderId,
                                           Optional<Integer> recipientId,
                                           Optional<String> amount,
                                           Optional<String> date) {

        try (Session session = sessionFactory.openSession()) {
            String queryString = getQueryForUser(userId,fromDate, toDate, senderId, recipientId, amount, date);

           Query<Transaction> query = session.createQuery(queryString,Transaction.class);

            query.setParameter("fromDate", fromDate.orElse(null));
            query.setParameter("toDate", toDate.orElse(null));
            query.setParameter("userId",userId);
            query.setParameter("senderId", senderId.orElse(0));
            query.setParameter("recipientId", recipientId.orElse(0));

            query.setFirstResult(pageable.getPageNumber());
            query.setMaxResults(pageable.getPageSize());

           List<Transaction>transactions=query.list();
            Page<Transaction> returnPage = new PageImpl<>(transactions);
            return returnPage;
        }

    }

//    @Override
//    public List<Transaction> getAllFiltered(int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date) {
//        try (Session session = sessionFactory.openSession()) {
//            String queryString = "from Transaction where (:senderId = 0 or sender.id = :senderId) and (:recipientId = 0 or recipient.id = :recipientId)  and (:fromDate = null or transactionDate >= :fromDate)  and (:toDate = null or transactionDate <= :toDate)";
//            if (amount.isPresent() && (amount.equals(Optional.of("asc")) || amount.equals(Optional.of("desc"))) && date.equals(Optional.of(""))) {
//                queryString += " order by amount " + amount.get();
//            }
//            if (date.isPresent() && (date.equals(Optional.of("asc")) || date.equals(Optional.of("desc"))) && amount.equals(Optional.of(""))) {
//                queryString += " order by transactionDate " + date.get();//order by amount asc, date desc
//            }
//            if (amount.isPresent() && date.isPresent()) {
//                if ((amount.equals(Optional.of("asc")) || amount.equals(Optional.of("desc"))) && (date.equals(Optional.of("asc")) || date.equals(Optional.of("desc")))) {
//                    queryString += " order by amount " + amount.get() + ", transactionDate " + date.get();
//                }
//            }
//            Query<Transaction> query = session.createQuery(queryString, Transaction.class);
//
//            query.setParameter("fromDate", fromDate.orElse(null));
//            query.setParameter("toDate", toDate.orElse(null));
//            query.setParameter("senderId", senderId.orElse(0));
//            query.setParameter("recipientId", recipientId.orElse(0));
//            return query.list();
//        }
//    }

}
