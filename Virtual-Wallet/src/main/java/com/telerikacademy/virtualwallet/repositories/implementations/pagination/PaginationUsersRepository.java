package com.telerikacademy.virtualwallet.repositories.implementations.pagination;

import com.telerikacademy.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PaginationUsersRepository extends JpaRepository<User, Long> {

    @Override
    Page<User> findAll( Pageable pageable);


}
