package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.models.Card;

import java.util.List;

public interface CardRepository {

    List<Card> getAll();

    Card getByUserId(int id);

    Card getByField(String field,String value);

    void create(Card card);

    void update(Card card);

    void delete(int id);
}
