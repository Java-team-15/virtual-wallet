package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.models.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository {

    Transaction getById(int id);

    Transaction getByField(String field, String value);

    void create(Transaction transaction);

    Page<Transaction> getAll( Pageable pageable);
    long getTotalElements();

    String getQueryForUser(int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date);

    long getTotalElements(int userId, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date);

    Page<Transaction> getAllForUser(int userId, Pageable pageable, Optional<LocalDate> fromDate, Optional<LocalDate> toDate, Optional<Integer> senderId, Optional<Integer> recipientId, Optional<String> amount, Optional<String> date);

}
