package com.telerikacademy.virtualwallet.repositories.implementations;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.models.VerificationToken;
import com.telerikacademy.virtualwallet.repositories.interfaces.VerificationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenRepositoryImpl implements VerificationTokenRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
   public void create(VerificationToken verificationToken){
       try (Session session = sessionFactory.openSession()) {

           session.save(verificationToken);
       }
   }

    @Override
    public VerificationToken getVerificationToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where (token = :token)", VerificationToken.class);
            query.setParameter("token", token);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "token:", token);
            }

            return query.list().get(0);
        }
    }
}
