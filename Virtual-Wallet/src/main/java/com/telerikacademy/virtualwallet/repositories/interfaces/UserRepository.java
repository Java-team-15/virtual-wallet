package com.telerikacademy.virtualwallet.repositories.interfaces;

import com.telerikacademy.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Page<User> getAll(Pageable pageable);

    User getById(int id);

    void create(User user);

    void update(User user);

    void delete(int id);

    User getByField(String field,String value);

    User getByAllFields(String value);

    Page<User> searchByKeyword (Pageable pageable,Optional<String> word);
    long getTotalElements(Optional<String> word);
    long getTotalElements();
}
