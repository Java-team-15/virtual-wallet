package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.models.Wallet;
import com.telerikacademy.virtualwallet.repositories.interfaces.WalletRepository;
import com.telerikacademy.virtualwallet.services.implementations.WalletServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class WalletServiceImplTests {
    @Mock
    WalletRepository mockRepository;

    @InjectMocks
    WalletServiceImpl service;

    @Test
    public void getById_should_returnWallet_when_matchExist() {
        // Arrange
        Wallet mockWallet = Helpers.createMockWallet();
        Mockito.when(mockRepository.getById(mockWallet.getId()))
                .thenReturn(mockWallet);
        // Act
        Wallet result = service.getById(mockWallet.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getId(), result.getId()),
                () -> Assertions.assertEquals(mockWallet.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockWallet.getBalance(), result.getBalance())
        );
    }
    @Test
    public void getByUserId_should_returnWallet_when_matchExist() {
        // Arrange
        Wallet mockWallet = Helpers.createMockWallet();

        Mockito.when(mockRepository.getByUserId(mockWallet.getUser().getId()))
                .thenReturn(mockWallet);
        // Act
        Wallet result = service.getByUserId(mockWallet.getUser().getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getId(), result.getId()),
                () -> Assertions.assertEquals(mockWallet.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockWallet.getBalance(), result.getBalance())
        );
    }
//    @Test
//    public void create_should_callRepository(){
//        // Arrange
//        Wallet mockWallet = Helpers.createMockWallet();
//
//        // Act
//        service.create(mockWallet.getUser());
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(mockWallet);
//    }
    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingWallet() {
        // Arrange
        Wallet mockWallet = Helpers.createMockWallet();


        // Act
        service.update(mockWallet);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockWallet);
    }


}
