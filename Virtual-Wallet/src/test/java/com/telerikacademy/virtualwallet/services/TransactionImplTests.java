package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.Transaction;
import com.telerikacademy.virtualwallet.models.User;
import com.telerikacademy.virtualwallet.repositories.interfaces.TransactionRepository;
import com.telerikacademy.virtualwallet.services.implementations.TransactionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class TransactionImplTests {
    @Mock
    TransactionRepository mockRepository;

    @InjectMocks
    TransactionServiceImpl service;

    @Test
    void getAll_should_callRepository_when_userIsAdmin() {
        // Arrange
        User mockAdmin = Helpers.createMockAdmin();

        // Act
        service.getAll(mockAdmin);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAll_should_throwException_when_initiatorIsNotAdmin() {
        // Arrange
        User mockInitiator = Helpers.createMockUser();
        mockInitiator.setUsername("MockInitiator");

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockInitiator));
    }

    @Test
    public void create_should_throwException_when_isNotUser() {
        // Arrange
        Transaction mockTransaction = Helpers.createMockTransaction();
        User mockInitiator = Helpers.createMockAdmin();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockTransaction, mockInitiator));
    }
//    @Test
//    void getAllForUser_should_callRepository() {
//        User mockUser = Helpers.createMockUser();
//        User mockInitiator = Helpers.createMockUser();
//        // Arrange
//        Mockito.when(mockRepository.getAllForUser(mockUser.getId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()))
//                .thenReturn(new ArrayList<>());
//
//        // Act
//        service.getAllForUser(mockInitiator, mockUser.getId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .getAllForUser(mockUser.getId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
//    }
    @Test
    void getAllForUser_should_throwException_when_initiatorIsNotOwner() {
        // Arrange
        User mockUser = Helpers.createMockUser();
        User mockInitiator = Helpers.createMockUser();
        mockInitiator.setId(2);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllForUser(mockInitiator, mockUser.getId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));
    }

//    @Test
//    void getById_should_throwException_when_initiatorIsNotAdmin() {
//        // Arrange
//        User mockUser = Helpers.createMockUser();
//        User mockInitiator = Helpers.createMockUser();
//
//        // Act, Assert
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.getAllForUser(mockInitiator, mockUser.getId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()));
//    }


}
