package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.Card;
import com.telerikacademy.virtualwallet.repositories.interfaces.CardRepository;
import com.telerikacademy.virtualwallet.services.implementations.CardServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class CardServiceImplTests {
    @Mock
    CardRepository mockRepository;

    @InjectMocks
    CardServiceImpl service;
    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }
    @Test
    public void getById_should_returnCategory_when_matchExist() {
        // Arrange
        Card mockCard = Helpers.createMockCard();

        Mockito.when(mockRepository.getByField("id",String.valueOf(mockCard.getId())))
                .thenReturn(mockCard);
        // Act
        Card result = service.getByField("id",String.valueOf(mockCard.getId()));

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCard.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCard.getCardNumber(), result.getCardNumber()),
                () -> Assertions.assertEquals(mockCard.getCardHolder(), result.getCardHolder()),
                () -> Assertions.assertEquals(mockCard.getCheckNumber(), result.getCheckNumber()),
                () -> Assertions.assertEquals(mockCard.getExpirationDate(), result.getExpirationDate()),
                () -> Assertions.assertEquals(mockCard.getCardType(), result.getCardType())
        );
    }
    @Test
    public void getByUserId_should_returnCard_when_matchExist() {
        // Arrange
        Card mockCard = Helpers.createMockCard();

        Mockito.when(mockRepository.getByUserId(mockCard.getUser().getId()))
                .thenReturn(mockCard);
        // Act
        Card result = service.getByUserId(mockCard.getUser().getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCard.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCard.getCardNumber(), result.getCardNumber()),
                () -> Assertions.assertEquals(mockCard.getCardHolder(), result.getCardHolder()),
                () -> Assertions.assertEquals(mockCard.getCheckNumber(), result.getCheckNumber()),
                () -> Assertions.assertEquals(mockCard.getExpirationDate(), result.getExpirationDate()),
                () -> Assertions.assertEquals(mockCard.getCardType(), result.getCardType())
        );
    }
    @Test
    public void create_should_throw_when_cardWithSameCardNumberExists() {
        // Arrange
        Card mockCard = Helpers.createMockCard();

        Mockito.when(mockRepository.getByField("card_number",mockCard.getCardNumber()))
                .thenReturn(mockCard);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCard));
    }
    @Test
    public void create_should_callRepository_when_CardWithSameCardNumberDoesNotExist() {
        // Arrange
        Card mockCard = Helpers.createMockCard();

        Mockito.when(mockRepository.getByField("card_number",mockCard.getCardNumber()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockCard);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCard);
    }

    @Test
    void delete_should_callRepository_when_matchExist() {
        // Arrange

        Card mockCard = Helpers.createMockCard();


        // Act
        service.delete(mockCard.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockCard.getId());
    }
}
