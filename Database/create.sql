create or replace table roles
(
    id int auto_increment,
    name varchar(10) not null,
    constraint roles_id_uindex
        unique (id)
);

alter table roles
    add primary key (id);

create or replace table users
(
    id int auto_increment,
    username varchar(20) not null,
    password varchar(20) not null,
    email varchar(30) not null,
    phone_number varchar(10) not null,
    status enum('BLOCK', 'UNBLOCK') not null,
    user_photo varchar(1024) null,
    role_id int not null,
    enabled tinyint(1) not null,
    constraint users_email_uindex
        unique (email),
    constraint users_id_uindex
        unique (id),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_roles_fk
        foreign key (role_id) references roles (id)
);

alter table users
    add primary key (id);

create or replace table cards
(
    id int auto_increment,
    card_number varchar(16) not null,
    card_holder varchar(30) not null,
    check_number varchar(3) not null,
    expiration_date date not null,
    user_id int not null,
    card_type enum('DEBIT', 'CREDIT') not null,
    constraint cards_id_uindex
        unique (id),
    constraint cards__users_fk
        foreign key (user_id) references users (id)
);

alter table cards
    add primary key (id);

create or replace table confirmation_tokens
(
    id int auto_increment,
    confirmation_token varchar(255) null,
    created_time datetime null,
    user_id int null,
    constraint confirmation_tokens_id_uindex
        unique (id),
    constraint confirmation_tokens_users_id_fk
        foreign key (user_id) references users (id)
);

alter table confirmation_tokens
    add primary key (id);

create or replace table pending_transactions
(
    id int auto_increment,
    sender_id int null,
    recipient_id int null,
    amount int not null,
    confirmation_token_id int null,
    constraint pending_transactions_id_uindex
        unique (id),
    constraint pending_transactions_confirmation_tokens_id_fk
        foreign key (confirmation_token_id) references confirmation_tokens (id),
    constraint pending_transactions_users_id_fk
        foreign key (recipient_id) references users (id),
    constraint pending_transactions_users_id_fk_2
        foreign key (sender_id) references users (id)
);

alter table pending_transactions
    add primary key (id);

create or replace table transactions
(
    id int auto_increment,
    transaction_date date not null,
    amount double(64,2) not null,
    sender_id int not null,
    recipient_id int not null,
    constraint transactions_id_uindex
        unique (id),
    constraint transactions__recipient_users_fk
        foreign key (recipient_id) references users (id),
    constraint transactions__users_fk
        foreign key (sender_id) references users (id)
);

alter table transactions
    add primary key (id);

create or replace table wallets
(
    id int auto_increment,
    amount double(64,2) not null,
    user_id int not null,
    constraint wallets_id_uindex
        unique (id),
    constraint wallets__users_fk
        foreign key (user_id) references users (id)
);

alter table wallets
    add primary key (id);

